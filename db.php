<?php
ini_set('display_errors', 1);
class db {
    // The database connection

    protected static $connection;


    public function connect() {
        // Try and connect to the database
        if(!isset(self::$connection))
        {
            self::$connection = new mysqli('localhost','teenpal','teenpal','teenpal');
        // self::$connection = new mysqli('localhost','root','bismillah','TopLines');
			mysqli_set_charset( self::$connection,"UTF8");
        }

        // If connection was not successful, handle the error
        if(self::$connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
            return false;
        }
	self::$connection->set_charset("utf8");
        return self::$connection;
    }

    
    public function query($query)
    {
        // Connect to the database
        $connection = $this -> connect();

        //echo $query;
        // Query the database
        $result = $connection -> query($query);

        return $result;
    }
    public function query3($query)
    {
	
        // Connect to the database
        $connection = $this -> connect();
	//mysqli_set_charset( $connection,"UTF8");
	//echo $connection;
        $result = $connection -> query($query);
       
        echo $result;

    }


    public function query4($query)
    {
	
        // Connect to the database
        $connection = $this -> connect();
	//mysqli_set_charset( $connection,"UTF8");
	//echo $connection;
        $result = $connection -> query($query);
       echo mysqli_error($connection);
        return $result;

    }

    public function query2($query)
    {
        // Connect to the database
        $connection = $this -> connect();
	//mysqli_set_charset( $connection,"UTF8");
        //echo $query;
        // Query the database
        $result = $connection -> query($query);

        if ($result === TRUE)
        {
            //echo  $connection->insert_id;
		echo "success";
            ///echo $connection-> query('SELECT * FROM ADMIN;');
        }
        else
        {
            echo "ERROR in insert";
        }
    }

    public function mulquery($query)
    {
        $connection = $this -> connect();
	//mysqli_set_charset( $connection,"UTF8");
        if (mysqli_multi_query($connection,$query))
        {
            do
            {
                if ($result=mysqli_store_result($connection))
                {
                    // Fetch one and one row
                    echo $result.'  -<<  ';
                    while ($row=mysqli_fetch_row($result))
                    {
                       echo $row[0];
                    }

                    mysqli_free_result($result);
                }
            }
            while (mysqli_next_result($connection));
        }
        else
        {
            echo 'ERROR';
        }

    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function select($query)
    {
	//echo $query;
        $rows = array();
        $result = $this -> query($query);
        if($result === false) 
	{
		//echo mysqli_error();
		echo $result;
            return false;
        }
	else
	{
		//echo $result;
		
	}
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return json_encode($rows);
    }

    /**
     * Fetch the last error from the database
     *
     * @return string Database error message
     */
    public function error() {
        $connection = $this -> connect();
	//mysqli_set_charset( $connection,"UTF8");
        return $connection -> error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function quote($value) {
        $connection = $this -> connect();
	//mysqli_set_charset( $connection,"UTF8");
        return "'" . $connection -> real_escape_string($value) . "'";
    }
}




?>
